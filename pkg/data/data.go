package data

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"
)

var data = make(map[string]string)

var mutex = &sync.Mutex{}

var lookup = func(key string) string {
	// some code to look up the value
	// ? - will it always return a value or nil?
	// ? - how long does it take to look it up
	if key == "foo" {
		return "ringer"
	}
	return fmt.Sprintf("hello-%d", rand.Int())
}

func refresh() {
	log.Println("refresh started")
	for key, value := range data {
		v := lookup(key)
		if v != value { // value has changed
			mutex.Lock()
			data[key] = v
			mutex.Unlock()
			log.Printf("[%s,%s <- %s] - updated\n", key, value, v)
		} else {
			log.Printf("[%s,%s] - no change\n", key, value)
		}
	}
	log.Println("refresh ended")
}

func Run(quit <-chan bool) {
	ticker := time.NewTicker(time.Second * 1).C
	defer log.Println("Done") // before leaving

	for {
		select {
		case <-quit:
			return
		case <-ticker:
			refresh()
		}
	}
}

func Get(key string) string { // private call - for testing
	// don't block on read
	v, ok := data[key]
	if !ok {
		// value not found
		v = lookup(key) // assuming it will always return a value, see ? below
		mutex.Lock()
		data[key] = v
		mutex.Unlock()
	}
	return v
}
