package data

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGet(t *testing.T) {
	// re-init map to be empty (if anything leftover from previous cases)
	data = make(map[string]string)

	// overwrite lookup function to always return "haba-haba"
	lookup = func(string) string {
		return "haba-haba"
	}

	v := Get("bar")
	// assert we got foo back
	assert.Equal(t, "haba-haba", v)

	// assert that our data contains [bar,foo] entry
	vv, ok := data["bar"]
	assert.True(t, ok)
	assert.Equal(t, "haba-haba", vv)

}

func TestRefresh(t *testing.T){
	// re-init map to be empty (if anything leftover from previous cases)
	data = make(map[string]string)

	// load map with some values
	data["foo"] = "bar"
	data["fooz"] = "booz"

	// overwrite lookup
	lookup = func(string)string{
		return "tada"
	}

	refresh()

	assert.Equal(t, 2, len(data))
	assert.Equal(t, "tada", data["foo"])
	assert.Equal(t, "tada", data["fooz"])
}