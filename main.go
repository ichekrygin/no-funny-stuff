package main

import (
	"log"
	"time"

	"github.com/ichekrygin/no-funny-stuff/pkg/data"
)

func main() {
	log.Println("Started")

	quit := make(chan bool)
	go data.Run(quit)

	data.Get("foo")
	// give some time
	time.Sleep(3 * time.Second)

	data.Get("bar")
	time.Sleep(3 * time.Second)

	quit <- true
	log.Println("Ended")
}
